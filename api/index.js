const express = require('express');
const cors = require('cors');
const routes = require('./app/routes');
const fileDb = require('./fileDb');

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/messages', routes);

fileDb.init();
app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});