import React from 'react';
import MessengerContainer from "./containers/MessengerContainer/MessengerContainer";

const App = () => {
    return (
        <>
            <MessengerContainer/>
        </>
    );
};

export default App;