import {
    CREATE_MESSAGE_FAILURE,
    CREATE_MESSAGE_REQUEST, CREATE_MESSAGE_SUCCESS,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS
} from "./actions";

const initialState = {
    messages: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST:
            return {...state};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messages: action.payload};
        case FETCH_MESSAGES_FAILURE:
            return {...state};
        case CREATE_MESSAGE_REQUEST:
            return {...state};
        case CREATE_MESSAGE_SUCCESS:
            return {...state};
        case CREATE_MESSAGE_FAILURE:
            return {...state};
        default:
            return state;
    }
}

export default reducer;