import React from 'react';
import {Card, makeStyles} from "@material-ui/core";
import {apiURL} from "../../apiUrl";

const useStyles = makeStyles({
    card: {
        marginBottom: "10px",
        background: "beige",
        padding: "10px"
    },
    image: {
        width: "100px",
        marginRight: "10px"
    }
})

const MessagesList = ({messages}) => {
    const classes = useStyles();

    return (
        <div>
            {messages && messages.map(message => (
                <Card key={message.id} className={classes.card}>
                    <div style={{display: "flex", alignItems: "center"}}>
                        {
                            message.image ?
                            <div className={classes.image}>
                                <img
                                    src={`${apiURL}/uploads/${message.image}`}
                                    alt=""
                                    style={{width: "100%"}}
                                />
                            </div> : null
                        }
                        <p><b>Author:</b> {message.author}</p>
                    </div>
                    <p><b>Message:</b> {message.message}</p>
                </Card>
            ))}
        </div>
    );
};

export default MessagesList;