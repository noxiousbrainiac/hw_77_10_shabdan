import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container} from "@material-ui/core";
import AppForm from "../../components/AppForm/AppForm";
import {createMessage, getMessages} from "../../store/actions";
import MessagesList from "../../components/MessagesList/MessagesList";

const MessengerContainer = () => {
    const dispatch = useDispatch();

    const {messages} = useSelector(state => state);

    const sendMessage = (message) => {
        dispatch(createMessage(message));
    }

    useEffect(() => {
        dispatch(getMessages());
    },[dispatch]);

    return (
        <Container>
            <AppForm
                send={sendMessage}
            />
            <MessagesList messages={messages}/>
        </Container>
    );
};

export default MessengerContainer;